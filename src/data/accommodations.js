export const Acco =
[{
	id: 1,
	name: 'Katala Suites',
	slug: 'katala-suites',
	desc: 'A Suites with an outdoor shared swimming pool suitable for low budged couple. Comfortable for 2 people and 1 children.',
	ldesc: `<p>Suites with garden view. A Suites with an outdoor shared swimming pool suitable for low budged couple. Comfortable for 2 people and 1 children.</p>
	<p>In suite service include free wifi, breakfast, private bathroom with water heater, tv cable, air conditioner</p>`,
	facilities: [
		'Garden view', 'Pool view', 'Landmark view', 'Telephone', 'Satellite Channels', 'Flat-screen TV', 'Safety Deposit Box', 'Air Conditioning', 'Desk', 'Seating Area', 'Fan', 'Free Wifi',
		'Sofa', 'Tile/Marble floor', 'Hardwood/Parquet floors', 'Mosquito net', 'Wardrobe/Closet', 'Cleaning products', 'Clothes rack', 'Shower', 'Hairdryer', 'Bathrobe', 'Free toiletries',
		'Toilet', 'Bathroom', 'Slippers', 'Electric kettle', 'Wake-up service', 'Towels', 'Linen'
	]
},
{
	id: 2,
	name: 'One Bedroom Villas',
	slug: 'one-bedroom-villas',
	desc: 'The One Bedroom Villa with a private swimming pool in 8 x 5 meters space suitable for honeymoon couple. Comfortable for 2 people and 1 children.',
	ldesc: `<p>The One Bedroom Villa with a private swimming pool in 8 x 5 meters space suitable for honeymoon couple. The villa completes with private kitchenette, living room and butler service. Comfortable for 2 people and 1 children.</p>
	<p>Each villa is comprised with a private swimming pool set within a sunny, walled courtyard garden, flowering plants and complemented by Balinese stone carvings. Pools are lined with Bali Green stone and bordered by palimanan stone; while bangkirai poolside sundecks. and furnished with sun loungers and umbrellas.</p>`,
	facilities: [
		'Free WiFi', 'Garden view', 'Pool view', 'Telephone', 'DVD Player', 'CD Player', 'Satellite Channels', 'Flat-screen TV', 'Safety Deposit Box', 'Air Conditioning', 'Desk', 'Seating Area', 'Fan',
		'Private entrance', 'Sofa', 'Tile/Marble floor', 'Hardwood/Parquet floors', 'Mosquito net', 'Private Pool', 'Wardrobe/Closet', 'Shower', 'Bath', 'Hairdryer', 'Bathrobe', 'Free toiletries', 'Toilet', 
		'Bathroom', 'Slippers', 'Tea/Coffee Maker', 'Minibar', 'Refrigerator', 'Microwave', 'Kitchen', 'Dining area', 'Electric kettle', 'Kitchenware', 'Oven', 'Stovetop', 'Toaster'
	]
},
{
	id: 3,
	name: 'Two Bedroom Villas',
	slug: 'two-bedroom-villas',
	desc: '2 bedroom villa with private swimming pool in 8 x 5 meters on 2 king size bed configuration suitable for family with 2 children',
	ldesc: `<p>2 bedroom villa with private swimming pool in 8 x 5 meters on 2 king size bed configuration suitable for family with 2 children. This villa also has it is private kitchenette, living room where butler service is available for guest, convenience & comfort for maximum of 4 adults, and a maximum of 2 children.</p>
	<p>Each villa is comprised with a private swimming pool set within a sunny, walled courtyard garden, flowering plants and complemented by Balinese stone carvings. Pools are lined with Bali Green stone and bordered by palimanan stone; while bangkirai poolside sundecks. and furnished with sun loungers and umbrellas.</p>`,
	facilities: [
		'Balcony', 'Garden view', 'Pool view', 'Telephone', 'DVD Player', 'CD Player', 'Satellite Channels', 'Flat-screen TV', 'Safety Deposit Box', 'Air Conditioning', 'Desk', 'Seating Area', 'Fan',
		'Private entrance', 'Sofa', 'Tile/Marble floor', 'Hardwood/Parquet floors', 'Mosquito net', 'Private Pool', 'Wardrobe/Closet', 'Shower', 'Bath', 'Hairdryer', 'Bathrobe', 'Free toiletries', 'Toilet',
		'Bathroom', 'Slippers', 'Tea/Coffee Maker', 'Minibar', 'Refrigerator', 'Microwave', 'Kitchen', 'Dining area', 'Electric kettle', 'Kitchenware', 'Oven', 'Stovetop', 'Toaster', 'Free WiFi'
	]
}]