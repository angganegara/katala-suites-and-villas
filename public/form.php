<?php
// allow form to be submitted from anywhere
header("Access-Control-Allow-Origin: *");

require_once 'lib/Rmail/Rmail.php';
error_reporting(0);

$fname = trim(htmlspecialchars($_POST['fname']));
$lname = trim(htmlspecialchars($_POST['lname']));
$email = trim(htmlspecialchars($_POST['email']));
$msg = trim(htmlspecialchars($_POST['message']));
$_POST['captchaText'] = strtoupper($_POST['captchaText']);
$captcha = strtoupper($_POST['captcha']);
$try = strtoupper($_POST['captchaText']);
$message = "
<p>Dear administrator,</p>
<p>You have received a message from katalasuitesandvillas.com contact form. Below is the detail:</p>
<br>
<p>
Name: ". $fname ." ". $lname ."<br>
Email: ". $email ."<br>
Message: ". nl2br($msg) ."</p>
<br>
<p>Thank you,</p>
<p>Katala Mailer</p>";

if ($fname != '' && $lname != '' && $email != '' && $msg != '' && ($captcha == $try)) {
	$mail = new Rmail();
	$mail->setFrom('Katala Mailer <no-reply@katalasuitesandvillas.com>');
	$mail->setSubject('Message from katalasuitesandvillas.com website');
	$mail->setPriority('high');
	$mail->setText(strip_tags($message));
	$mail->setHTML($message);
	$mail->send(array('info@katalasuitesandvillas.com', 'dispedia@gmail.com'));
	//$mail->send(array('info@katalasuitesandvillas.com'));
	echo 'OK';
	exit;
} else {
	echo 'Cannot send messages';
	exit;
}