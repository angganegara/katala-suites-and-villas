// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import './assets/scss/app.scss'
import '../node_modules/lightbox2/dist/css/lightbox.min.css'

Vue.config.productionTip = false
window.$ = require('jquery')
window._ = require('lodash')

new Vue({
	el: '#app',
	router,
	template: '<App/>',
	components: { App }
})