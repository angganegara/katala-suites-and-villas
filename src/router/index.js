import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import About from '@/components/About'
import Accommodations from '@/components/Accommodations/Index'
import Accommodation from '@/components/Accommodations/View'
import Rates from '@/components/Rates'
import Contact from '@/components/Contact'
import Gallery from '@/components/Gallery'

Vue.use(Router)

export default new Router({
	mode: 'history',
	routes: [
		{
			path: '/',
			name: 'Home',
			component: Home
		},
		{
			path: '/pages/about-us',
			name: 'About',
			component: About
		},
		{
			path: '/accommodations',
			name: 'Accommodations',
			component: Accommodations
		},
		{
			path: '/accommodations/:id/:slug',
			name: 'Accommodation',
			component: Accommodation
		},
		{
			path: '/pages/rates-and-services',
			name: 'RatesAndServices',
			component: Rates
		},
		{
			path: '/pages/contact',
			name: 'Contact',
			component: Contact
		},
		{
			path: '/gallery',
			name: 'Gallery',
			component: Gallery
		}
	],
	scrollBehavior(to, from, savedPosition) {
		if (savedPosition) {
			return savedPosition
		} else {
			return { x: 0, y: 0 }
		}
	}
})
