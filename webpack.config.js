module.exports = {
	module: {
		rules: [{
			test: /\.scss$/,
			use: [{
				loader: 'style-loader'
			},{
				loader: 'css-loader'
			},{
				loader: 'sass-loader',
				options: {
					includePaths: ['src/assets/scss']
				}
			}]
		}, {
			test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
			loader: 'url'
		}]
	}
}